/*
 Helper functions. These may be executed as a GitHub worker inside AppMaker and
 later replaced by an AppMaker API call here.
 */

var GitHub = require('./github');
var redis = require('redis'),
  client = redis.createClient();
var config;

if (process.env.NODE_ENV == 'production') {
  console.log("Starting in production mode...");
  config = require('./config.json');
} else {
  console.log("Starting in development mode...");
  config = require('./config.dev.json');
}


/*
 Params for managing OAUTH2 connections against GH
 */
const GH_PARAMS = config.GH_PARAMS;
const TOKEN_LENGTH = GH_PARAMS.token_length;
const TOKEN_HASH = GH_PARAMS.token_hash;


function generateToken(access_token, userData) {
  console.log('Generating token for', access_token);
  return new Promise(function(resolve, reject) {
    function randomInt (low, high) {
      return Math.floor(Math.random() * (high - low) + low);
    }

    var start = randomInt(0, access_token.length - TOKEN_LENGTH);

    var token = access_token.substr(start, TOKEN_LENGTH);
    userData.access_token = access_token;
    var strUserData = JSON.stringify(userData);

    client.hset(TOKEN_HASH, token, strUserData , function(error, result) {
      if (error) {
        reject(error);
        return;
      }
      console.log('Token generated: ', token);
      // Associating the userName to the access_token
      client.hset(TOKEN_HASH, userData.userName, access_token,
        function(error, result) {
          if (error) {
            reject(error);
            return;
          }
          resolve(token);
        });
    });
  });
}

function removeSessionData(req) {
  return new Promise(function(resolve, reject) {
    var token = req.cookies.token;

    client.hget(TOKEN_HASH, token, function(error, result) {
      if (error || !result) {
        reject(error);
        return;
      }
      var userName = JSON.parse(result).userName;
      client.hdel(TOKEN_HASH, userName, function(error, result) {
        if (error) {
          reject(error);
          return;
        }
        client.hdel(TOKEN_HASH, token, function(error, result) {
          if (error) {
            reject(error);
            return;
          }
          resolve();
        });
      });
    });
  });
}

function getSessionData(token) {
  return new Promise(function(resolve, reject) {
    client.hget(TOKEN_HASH, token, function(error, result) {
      if (error) {
        reject(error);
        return;
      }

      resolve(JSON.parse(result));
    });
  });
}

function addManifestFile(repoName, owner, access_token) {
  console.log('Going to add manifest file ...');

  var manifest = {
    name: repoName,
    description: 'Automatically generated',
    icons: {
      '84': "/icons/sms_84.png",
      '126': "/icons/sms_126.png",
      '142': "/icons/sms_142.png",
      '189': "/icons/sms_189.png",
      '284': "/icons/sms_284.png"
    }
  };

  var content = new Buffer(JSON.stringify(manifest));

  return GitHub.addFile(repoName, owner, 'gh-pages', 'manifest.json',
    content, access_token);
}

function copyIcon(iconPath, repoName, owner, access_token) {
  // TODO Admitir array de iconPaths
  console.log('Going to add icons ...');

  return new Promise(function(resolve, reject) {
    fs.readFile(__dirname + '/resources/' + iconPath, function(error, data) {
      if (error) {
        console.error('Error while reading file: ', error);
        reject(error);
        return;
      }
      GitHub.addFile(repoName, owner, 'gh-pages', iconPath,
        new Buffer(data, 'binary'), access_token).then(resolve, reject);
    });
  });
}

function createGhPages(repoName, owner, sha) {
  console.log('Going to create gh-pages branch ...', sha);

  // First it is needed to get the access_token
  client.hget(TOKEN_HASH, owner, function(error, access_token) {
    if (error) {
      console.error('Cannot get access_token for: ', owner);
      return;
    }

    GitHub.updateBranch(repoName, owner, 'gh-pages', sha, access_token).then(
      function() {
        console.log('GH Pages branch created!!!', typeof addManifestFile,
          access_token);
        return addManifestFile(repoName, owner, access_token);
      },
      function() {
        console.error('Error while creating the gh-pages branch');
      }
    ).then(function() {
        console.log('Everything went ok while creating GH pages');
        return copyIcon('icons/sms_84.png', repoName, owner, access_token);
      }).then(function() {
        return copyIcon('icons/favicon.ico', repoName, owner, access_token);
      });
  });
}


module.exports = {
  generateToken: generateToken,
  removeSessionData: removeSessionData,
  getSessionData: getSessionData,
  createGhPages: createGhPages,
  config: config,
  listRepos: GitHub.listRepos,
  subscribeRepo: GitHub.subscribeRepo,
  unsubscribeRepo: GitHub.unsubscribeRepo,
  fakePush: GitHub.fakePush,
  getUserData: GitHub.getUserData
};
