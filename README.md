GH_Autodeployer
===============

This module auto-deploys Web Apps from a GH repository

Documentation
-------------

You can check the documentation for this project's REST API in the `/docs` folder.


TODO
----

[ ] Change API keys to a GH app under OWD team.


Deploy script
-------------

		#!/bin/sh

		NODE_ENV=production nohup supervisor --harmony --no-restart-on error --watch /home/jmcf/github /home/jmcf/github/server.js &

For testing:
		
		NODE_ENV=production nohup node-v0.12.0-linux-x64/bin/node --harmony server.js &


Repo link
---------

Originally from: `https://pdihub.hi.inet/jmcf/GH_Autodeployer`.
Forked to: `TODO`


Deploy server
-------------

		ssh cacheator@owd.tid.ovh

Assigned http port: `8082`

The app is running at http://owd.tid.ovh:8082/
