// Copyright (c) 2015 Telefónica I+D S.A.U.
// Author: José Manuel Cantera Fonseca (jmcf@tid.es)

'use strict';

// Dependencies
var URL = require('url');
var QueryString = require('querystring');
var Request = require('request');
var fs = require('fs');
var utils = require('./autodeployer-utils');
var config = utils.config;
var express = require('express');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


/*
 Params for managing OAUTH2 connections against GH
*/
const GH_PARAMS = config.GH_PARAMS;


/*
 App configuration
*/
var app = express();

var loggerStream = fs.createWriteStream('./log.txt', {
  flags: 'a',
  encoding: 'utf8',
  mode: '0666'
});

app.use(morgan('dev',{
  stream: loggerStream
}));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.static(process.cwd() + '/public'));


/*
 HTTP routes
*/

app.get('/repo_list', function(req,resp) {
  var token = req.cookies.token;

  if (!token) {
    console.log('No token found, error message.');
    resp.status(401).json({
      'status': 'error',
      'message': 'No user seems to be logged in.',
      'url': config.URL + '/auth'
    });
    return;
  }

  console.log('Token found: ', token);

  var sessionData;

  utils.getSessionData(token).then(function(sData) {
    sessionData = sData;

    if (!sessionData) {
      resp.status(401).json({
        'status': 'error',
        'message': 'No user seems to be logged in.',
        'url': config.URL + '/auth'
      });
      return Promise.reject();
    }

    console.log('From the session data the userName is: ',
      sessionData.userName);

    return utils.listRepos(sessionData.userName);
  }).then(function(repos) {
    console.log('Repos ready ...');
    resp.json({
      'status': 'OK',
      'userName': sessionData.userName,
      'repos': repos
    });
  }, function rejected() {
  });
});


app.post('/subscribe/:repoId', function(req, resp) {
  let repoId = req.params.repoId;

  if (!repoId) {
    resp.sendStatus(404);
    return;
  }

  let token = req.cookies.token;

  if (!token) {
    resp.status(401).json({
      'status': 'error',
      'message': 'No user seems to be logged in.',
      'url': config.URL + '/auth'
    });
    return;
  }

  utils.getSessionData(token).then(function(sessionData) {
    if (!sessionData) {
      resp.redirect('/auth');
      return Promise.reject();
    }

    return utils.subscribeRepo(repoId, sessionData.userName,
                                sessionData.access_token);
  }).then(function() {
      console.log('Repo subscribed successfully: ', repoId);
      resp.json({
        'status': 'OK'
      });
  }, function rejected(err) {
      console.log('Cannot subscribe to GH repo: ', err.name);
      resp.status(500).json({
        'status': 'error',
        'message': err.name
      });
  });

});

app.post('/unsubscribe/:repoId', function(req, resp) {
  let repoId = req.params.repoId;

  if (!repoId) {
    resp.status(404).json({
      'status': 'error',
      'message': 'The repository ' + repoId + ' wasn\'t found.'
    });
    return;
  }

  let token = req.cookies.token;

  if (!token) {
    resp.status(401).json({
      'status': 'error',
      'message': 'No user seems to be logged in.',
      'url': config.URL + '/auth'
    });
    return;
  }

  utils.getSessionData(token).then(function(sessionData) {
    if (!sessionData) {
      resp.status(401).json({
        'status': 'error',
        'message': 'No session data found in server. Please log in again.',
        'url': config.URL + '/auth'
      });
      return Promise.reject();
    }

    return utils.unsubscribeRepo(repoId, sessionData.userName,
                                sessionData.access_token);
  }).then(function() {
      console.log('Repo unsubscribed successfully: ', repoId);
      resp.json({
        'status': 'OK'
      });
  }, function rejected(err) {
      console.log('Cannot unsubscribe to GH repo: ', JSON.stringify(err));
      resp.json({
        'status': 'error',
        'message': 'Cannot unsubscribe to GH repo: ' + JSON.stringify(err)
      });
  });
});

app.post('/fake_push/:repoId', function(req, resp) {
  let repoId = req.params.repoId;

  if (!repoId) {
    resp.sendStatus(404);
    return;
  }

  let token = req.cookies.token;

  if (!token) {
    resp.status(401).send({
      'status': 'error',
      'message': 'No user seems to be logged in.',
      'url': config.URL + '/auth'
    });
    return;
  }

  utils.getSessionData(token).then(function(sessionData) {
    if (!sessionData) {
      resp.redirect('/auth');
      return Promise.reject();
    }

    return utils.fakePush(repoId, sessionData.userName,
                                sessionData.access_token);
  }).then(function() {
      console.log('Fake push to repo sent: ', repoId);

      resp.type('json');
      resp.send({
        'status': 'OK'
      });
  }, function rejected(err) {
      console.log('Cannot send fake push to repo: ', err.name);
      console.log(JSON.stringify(err));
      resp.send({
        'status': err.name
      });
  });

});

// It seems GH OAUTH tokens do not expire (but they could be revoked)
app.get('/auth', function(req, resp) {
  let params = {
    'client_id': GH_PARAMS.client_id,
    'state': GH_PARAMS.state,
    'redirect_uri': GH_PARAMS.redirect_uri,
    'scope': GH_PARAMS.scope
  };

  let loginPage = GH_PARAMS.login_page + '?' + QueryString.stringify(params);
  console.log('Login page: ', loginPage);
  resp.redirect(loginPage);
});

app.get('/logout', function(req, resp) {
  utils.removeSessionData(req).then(function() {
    resp.cookie('token', '');
    resp.type('json');
    resp.send({
      'status': 'OK'
    });
  });
});


/*
 GitHub OAUTH related HTTP routes
 */
// OAuth2 CB (Maybe move it to the client?)
app.get('/auth_cb', function(req, resp) {
  var url = URL.parse(req.originalUrl);
  var queryParams = QueryString.parse(url.query);

  if (queryParams.state !== GH_PARAMS.state) {
    resp.sendStatus(404);
    return;
  }

  console.log('State, Code: ', queryParams.state, queryParams.code);

  var params = {
    'client_id': GH_PARAMS.client_id,
    'client_secret': GH_PARAMS.client_secret,
    'code': queryParams.code,
    'redirect_uri': GH_PARAMS.redirect_uri
  };

  // Now the access_token is obtained calling the AUTH Server
  Request({
    method: 'POST',
    url: GH_PARAMS.auth_server,
    headers: {
      'User-Agent': 'Mozilla',
      'Accept': 'application/json',
      'Content-Type' : 'application/x-www-form-urlencoded'
    },
    body: QueryString.stringify(params)
  }, function(error, response, body) {
    if (error) {
      console.error('Error while getting the access_token: ', error);
      resp.sendStatus(500);
      return;
    }

    console.log(body);

    var data = JSON.parse(body);
    if (body.error) {
      console.error('Error while getting the access_token: ', error);
      resp.sendStatus(500);
      return;
    }

    var access_token = data.access_token;

    utils.getUserData(access_token).then(function(userData) {
      console.log("Retrieved user data:", userData);
      return utils.generateToken(access_token, userData);
    }, function(error) {
      console.error('Error while getting user data: ', error);
      resp.sendStatus(500);
    }).then(function(token) {
      console.log('Generated Token: ', token);
      resp.cookie('token', token);
      resp.redirect('/');
    });
  });
});

// This will be invoked by GitHub
app.post(config.SERVER.HOOK_HANDLER, function(req,resp) {
  console.log('GitHub hook has been invoked!!!!');

  var eventType = req.header('X-Github-Event');
  var eventData = req.body;

  // console.log(eventData);

  if (eventType === 'push' && eventData.ref.indexOf('master') !== -1) {
    var repository = eventData.repository;
    var repoName = repository.name;
    var owner = repository.owner.name;

    var sha = eventData.head_commit.id;

    console.log('A push happened over master!!!!!', repoName, owner);

    process.nextTick(utils.createGhPages.bind(null, repoName, owner, sha));
  }

  resp.sendStatus(200);
});


/*
  Start server.
 */
var server = app.listen(8082, function() {
  console.log('GitHub prototype server up and running on port %d', server.address().port);
});
