'use strict';

/*
 *  Allows to use GH by calling the GH API
 *
 *  This module uses ES6 features
 *
 */

var Request = require('request');
var config = require('./config.json');

const GH_SERVICE = config.GH.SERVICE;
const GH_USER = config.GH.USER;
const GH_REPOS = config.GH.REPOS;

function listRepos(userName) {
  let serviceURI = GH_SERVICE + GH_USER + userName + GH_REPOS;

  console.log('Service URI: ', serviceURI);

  return new Promise(function(resolve, reject) {
    Request({
      method: 'GET',
      url: serviceURI,
      headers: {
        'Accept': 'application/json',
        'User-Agent': 'Mozilla'
      },
      json:true
    }, function(error, response, body) {
        if (error) {
          reject(error);
          return;
        }

        console.log('Response ready: ', typeof body, response.statusCode);
        var apiResponse = body;
        var repoList = [];
        apiResponse.forEach(function(aRepo) {
          repoList.push({
            id: aRepo.id,
            name: aRepo.name,
            owner: aRepo.owner.login,
            html_url: aRepo.html_url
          });
        });
        resolve(repoList);
    })
  });
}

function getUserData(access_token) {
  return new Promise(function(resolve, reject) {
    Request({
      method: 'GET',
      url: GH_SERVICE + 'user',
      headers: {
        'Accept': 'application/json',
        'User-Agent': 'Mozilla'
      },
      qs: {
        access_token: access_token
      }
    }, function(error, response, body) {
        if (error) {
          reject(error);
          return;
        }
        var serviceUserData = JSON.parse(body);
        resolve({
          userId: serviceUserData.id,
          userName: serviceUserData.login
        });
    });
  });
}

// Tests if a repo is already subscribed
function isSubscribed(repoName, owner, access_token) {
  return listHooks(repoName, owner, access_token).then(function(hooks) {
    return Promise.resolve(hooks.length > 0);
  });
}

function listHooks(repoName, owner, access_token) {
  return new Promise(function(resolve, reject) {
    var service = GH_SERVICE + 'repos' + '/' + owner +
                                          '/' + repoName + '/hooks';
    Request({
      method: 'GET',
      url: service,
      headers: {
        'Accept': 'application/json',
        'User-Agent': 'Mozilla'
      },
      json: true,
       qs: {
        access_token: access_token
      }
    }, function(error, response, body) {
        if(error) {
          reject(error);
          return;
        }
        var hookList = body;
        var out = [];
        hookList.forEach(function(aHook) {
          if (aHook.config.url.indexOf('owd.tid.ovh') !== -1) {
            out.push(aHook);
          }
        });

        resolve(out);
    });
  });
}

// Subscribes the repo by installing a hook
function subscribeRepo(repoName, owner, access_token) {
  console.log('Going to subscribe repo: ', repoName, owner, access_token);

  var service = GH_SERVICE + 'repos' + '/' + owner + '/' + repoName + '/hooks';
  var hookData = {
    name: 'web',
    config: {
      url: config.URL + config.SERVER.HOOK_HANDLER,
      content_type: 'json'
    },
    events: ['push'],
    active: true
  };

  return new Promise(function(resolve, reject) {
    isSubscribed(repoName, owner, access_token).then(function(result) {
      if (result) {
        reject({
          name: 'AlreadyExists'
        });
        return;
      }
      Request({
        method: 'POST',
        url: service,
        headers: {
          'Accept': 'application/json',
          'User-Agent': 'Mozilla',
          'Content-Type': 'application/json'
        },
        qs: {
          access_token: access_token
        },
        body: hookData,
        json: true
      }, function(error, response, body) {
          if (error) {
            reject(error);
            return;
          }

          console.log('Response from the hook creation: ', typeof body, body);

          if (response.statusCode === 201) {
            console.log('Web hook created successfully');
            resolve();
          }
          else {
            reject({
              name: 'UnknownError'
            });
          }
      });
    }, reject);
  });
}

function unsubscribeRepo(repoName, owner, access_token) {
  console.log('Going to unsubscribe repo: ', repoName, access_token);

  return listHooks(repoName, owner, access_token).then(function(hooks) {
    // It is assumed only one hook we need to remove
    return removeHook(repoName, owner, hooks[0].id, access_token);
  });
}

function removeHook(repoName, owner, hookId, access_token) {
  var service = GH_SERVICE + 'repos' + '/' + owner + '/'
                + repoName + '/hooks' + '/' + hookId;

  return new Promise(function(resolve, reject) {
    console.log('Going to remove hook: ', hookId);

    Request({
        method: 'DELETE',
        url: service,
        headers: {
          'Accept': 'application/json',
          'User-Agent': 'Mozilla',
          'Content-Type': 'application/json'
        },
        qs: {
          access_token: access_token
        },
        json: true
      }, function(error, response, body) {
          if (error) {
            console.log('Error while removing hook: ', error);
            reject(error);
            return;
          }

          if (response.statusCode === 204) {
            console.log('Hook: ', hookId, 'deleted');
            resolve();
          }
          else {
            reject({
              name: 'CannotDelete'
            });
          }
      }
    );
  });
}

// Sends a fake push to the specified hook
function testHook(repoName, owner, hookId, access_token) {
  var service = GH_SERVICE + 'repos' + '/' + owner + '/'
                + repoName + '/hooks' + '/' + hookId + '/test';

  return new Promise(function(resolve, reject) {
    console.log('Going to push to hook: ', hookId);

    Request({
        method: 'POST',
        url: service,
        headers: {
          'Accept': 'application/json',
          'User-Agent': 'Mozilla',
          'Content-Type': 'application/json'
        },
        qs: {
          access_token: access_token
        },
        json: true
      }, function(error, response, body) {
          if (error) {
            console.log('Error while pushing to hook: ', error);
            reject(error);
            return;
          }

          if (response.statusCode === 204) {
            console.log('Hook: ', hookId, 'pushed');
            resolve();
          }
          else {
            reject({
              name: 'CannotPush'
            });
          }
      }
    );
  });
}

function fakePush(repoName, owner, access_token) {
  console.log('Going to push repo: ', repoName, access_token);

  return listHooks(repoName, owner, access_token).then(function(hooks) {
    // It is assumed only one hook we need to remove
    return testHook(repoName, owner, hooks[0].id, access_token);
  });
}

function createBranch(repoName, owner, branchName, sha, access_token) {
  var service = GH_SERVICE + 'repos' + '/' + owner + '/'
                + repoName + '/git' + '/' + 'refs';

  var branchData = {
    ref: 'refs/heads/' + branchName,
    sha: sha
  };

  return new Promise(function(resolve, reject) {
    Request({
      method: 'POST',
      url: service,
      headers: {
        'Accept': 'application/json',
        'User-Agent': 'Mozilla',
        'Content-Type': 'application/json'
      },
      qs: {
        access_token: access_token
      },
      body: branchData,
      json: true
    }, function(error, response, body) {
        if (error) {
          console.log('Error while pushing to hook: ', error);
          reject(error);
          return;
        }

        if (response.statusCode === 201) {
          console.log('Branch: ', branchName, 'created');
          resolve();
        }
        else {
          reject({
            name: 'CannotCreate'
          });
        }
    }
    );
  });
}

function deleteBranch(repoName, owner, branchName, access_token) {
  var service = GH_SERVICE + 'repos' + '/' + owner + '/'
                + repoName + '/git' + '/' + 'refs'
                + '/heads' + '/' + branchName;

  return new Promise(function(resolve, reject) {
    Request({
      method: 'DELETE',
      url: service,
      headers: {
        'Accept': 'application/json',
        'User-Agent': 'Mozilla'
      },
      qs: {
        access_token: access_token
      },
      json: true
    }, function(error, response, body) {
        if (error) {
          console.log('Error while deleting branch: ', branchName, error);
          reject(error);
          return;
        }

        if (response.statusCode === 204) {
          console.log('Branch: ', branchName, 'deleted');
          resolve();
        }
        else {
          reject({
            name: 'CannotDelete'
          });
        }
    }
    );
  });
}

// For updating a branch we delete it and then create it with the new SHA
function updateBranch(repoName, owner, branchName, sha, access_token) {
  // We try to delete the branch. This could fail if it does not exist
  return new Promise(function(resolve, reject) {
    deleteBranch(
      repoName, owner, branchName, access_token).then(function() {
      console.log('Branch deleted ... now recreating');
      return createBranch(repoName, owner, branchName, sha, access_token);
    }, function() {
        console.log('It seems the branch did not exist');
        createBranch(
          repoName, owner, branchName, sha, access_token).then(resolve, reject);
    }).then(resolve, reject);
  });
}

// content is a buffer
function addFile(repoName, owner, branchName, path, content, access_token) {
  console.log('File to be added: ', repoName, owner, branchName, path,
              content, access_token);
  var service = GH_SERVICE + 'repos' + '/' + owner + '/' + repoName + '/' +
                'contents/' + path;

  var contentData = {
    path: path,
    message: 'Automatically added by Autodeployer',
    content: content.toString('base64'),
    branch: branchName
  };

  return new Promise(function(resolve, reject) {
    console.log('Invoking service: ', service);

    Request({
      method: 'PUT',
      url: service,
      headers: {
        'Accept': 'application/json',
        'User-Agent': 'Mozilla',
        'Content-Type': 'application/json'
      },
      qs: {
        access_token: access_token
      },
      body: contentData,
      json: true
    }, function(error, response, body) {
        console.log('Response from the add file request ...');

        if (error) {
          console.log('Error while adding file: ', path);
          reject(error);
          return;
        }

        if (response.statusCode === 201) {
          console.log('Content: ', path, 'created');
          resolve();
        }
        else {
          console.error('File cannot be created: ', response.statusCode);
          reject({
            name: 'CannotAddFile'
          });
        }
    });
  });
}

exports.listRepos = listRepos;
exports.getUserData = getUserData;
exports.subscribeRepo = subscribeRepo;
exports.unsubscribeRepo = unsubscribeRepo;
exports.fakePush = fakePush;
exports.createBranch = createBranch;
exports.updateBranch = updateBranch;
exports.deleteBranch = deleteBranch;
exports.addFile = addFile;
